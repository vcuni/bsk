package tdd.training.bsk;


import java.util.ArrayList;
import java.util.List;

public class Game {

	List<Frame> frames;
	int firstBonusThrow;
	int secondBonusThrow;
	boolean lastspare;
	boolean laststrike;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>();
		firstBonusThrow = 0;
		secondBonusThrow = 0;
		lastspare = false;
		laststrike = false;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size()>=10) throw new BowlingException("game completed");
		frames.add(frame);
		if(frames.size()==10 && frames.get(9).isSpare()) lastspare = true;
		if(frames.size()==10 && frames.get(9).isStrike()) laststrike = true;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(!lastspare && !laststrike) throw new BowlingException("Final frame isn't a spare");
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(!laststrike) throw new BowlingException("Final frame isn't a strike");
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score=0;
		for(int i=0; i<frames.size();i++) {
			if(i==9 && lastspare) {
				score+=getFirstBonusThrow();
			}
			else if(i==9 && laststrike) {
				score+=getFirstBonusThrow()+getSecondBonusThrow();
			}
			else if(frames.get(i).isStrike()) {
				int j=i;
				do {
					j++;
					score+=frames.get(j).getScore();
				}while(frames.get(j).isStrike() && j<i+2 && j<9);
				if(i==7 && frames.get(j).isStrike()) score+=getFirstBonusThrow();
			}
			else if(frames.get(i).isSpare()) {
				frames.get(i).setBonus(frames.get(i+1).firstThrow);
			}
			score+=frames.get(i).getScore();
		}
		return score;	
	}

}
