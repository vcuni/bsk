package tdd.training.bsk;

import java.util.Objects;

public class Frame {

	int firstThrow;
	int secondThrow;
	int bonus;
	boolean spare;
	boolean strike;
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		spare = false;
		strike = false;
		bonus = 0;
		if(firstThrow+secondThrow>10) throw new BowlingException("incorrected score");
		if(firstThrow==10 || secondThrow==10) strike=true;
		if(firstThrow+secondThrow==10 && !strike) spare = true;
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 * @throws BowlingException 
	 */
	public void setBonus(int bonus) throws BowlingException {
		if((!strike && !spare) || bonus>10) throw new BowlingException("bonus error");
		this.bonus=bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 * @throws BowlingException 
	 */
	public int getBonus() throws BowlingException {
		if(!spare) throw new BowlingException("bonus error");
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return firstThrow+secondThrow+bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return strike;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return spare;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bonus, firstThrow, secondThrow, spare, strike);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frame other = (Frame) obj;
		return bonus == other.bonus && firstThrow == other.firstThrow && secondThrow == other.secondThrow
				&& spare == other.spare && strike == other.strike;
	}

}
