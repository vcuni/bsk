package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void initFrametest() throws BowlingException{
		Frame f = new Frame(1,6);
		assertEquals(f.getFirstThrow(),1);
		assertEquals(f.getSecondThrow(),6);
	}
	
	@Test
	public void getScoretest() throws BowlingException{
		Frame f = new Frame(1,6);
		assertEquals(f.getScore(),7);
	}
	
	@Test
	public void sparetest() throws BowlingException{
		Frame f = new Frame(5,5);
		f.setBonus(2);
		assertEquals(2, f.getBonus());
		assertEquals(f.getScore(),12);
		assertEquals(true, f.isSpare());
	}
	
	@Test
	public void striketest() throws BowlingException{
		Frame f = new Frame(10,0);
		assertEquals(true, f.isStrike());
	}

}
