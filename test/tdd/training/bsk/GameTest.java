package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	public void genFrame(int j, Game g) throws BowlingException {
		for(int i=0;i<j;i++) {
			Frame f = new Frame(1,6);
			g.addFrame(f);
		}
	}
	
	@Test
	public void addFrametest() throws BowlingException {
		Game g = new Game();
		Frame f = new Frame(1,6);
		g.addFrame(f);
		assertEquals(f,g.getFrameAt(0));
	}
	
	@Test
	public void calculateScoretest() throws BowlingException {
		Game g = new Game();
		genFrame(2, g);
		Frame f2 = new Frame(2,6);
		Frame f3 = new Frame(3,6);
		g.addFrame(f2);
		g.addFrame(f3);
		assertEquals(g.calculateScore(), 31);
	}
	
	@Test
	public void calculateScoreWithSparetest() throws BowlingException {
		Game g = new Game();
		for(int i=0;i<5;i++) {
			Frame f = new Frame(1,6);
			g.addFrame(f);	
		}
		Frame f6 = new Frame(4,6);
		g.addFrame(f6);
		genFrame(4, g);
		assertEquals(g.calculateScore(), 74);
	}
	
	@Test
	public void calculateScoreWithStrikeAndSparetest() throws BowlingException{
		Game g = new Game();
		genFrame(4, g);
		Frame f5 = new Frame(4,6);
		Frame f6 = new Frame(10,0);
		Frame f7 = new Frame(0,10);
		Frame f8 = new Frame(4,6);
		Frame f9 = new Frame(2,8);
		Frame f10 = new Frame(1,5);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		assertEquals(g.calculateScore(), 127);
	}
	
	@Test
	public void calculateScoreWithLastSparetest() throws BowlingException{
		Game g = new Game();
		genFrame(4, g);
		Frame f5 = new Frame(4,6);
		Frame f6 = new Frame(10,0);
		Frame f7 = new Frame(0,10);
		Frame f8 = new Frame(4,6);
		Frame f9 = new Frame(2,8);
		Frame f10 = new Frame(5,5);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		g.setFirstBonusThrow(1);
		assertEquals(g.calculateScore(), 136);
	}
	
	@Test
	public void calculateScoreWithLastStriketest() throws BowlingException{
		Game g = new Game();
		genFrame(4, g);
		Frame f5 = new Frame(4,6);
		Frame f6 = new Frame(10,0);
		Frame f7 = new Frame(0,10);
		Frame f8 = new Frame(4,6);
		Frame f9 = new Frame(2,8);
		Frame f10 = new Frame(0,10);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		g.setFirstBonusThrow(1);
		g.setSecondBonusThrow(1);
		assertEquals(g.calculateScore(), 132);
	}

	@Test
	public void calculateScoreWithAllStrikestest() throws BowlingException{
		Game g = new Game();
		Frame f = new Frame(10,0);
		for(int i=0; i<10;i++) g.addFrame(f);
		g.setFirstBonusThrow(10);
		g.setSecondBonusThrow(10);
		int score = 300;
		assertEquals(g.calculateScore(), score);
	}
}
